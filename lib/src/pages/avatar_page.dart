import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Avatar Page"),
        actions: <Widget>[
          Container(
            padding: EdgeInsets.all(5.0),
            child: CircleAvatar(
              backgroundImage: NetworkImage(
                  'https://image.dhgate.com/0x0/f2/albu/g7/M01/93/95/rBVaSVu1s7iASxhZAALKNNrmuzA524.jpg'),
              radius: 25.0,
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 10.0),
            child: CircleAvatar(
              child: Text('Sl'),
              backgroundColor: Colors.brown,
            ),
          )
        ],
      ),
      body: Center(
        child: FadeInImage(
          placeholder: AssetImage('assets/jar-loading.gif'),
          image: NetworkImage(
              'https://rlv.zcache.es/posavasos_de_piedra_tigre_blanco_con_ojos_azules-r0d8d3fac51804552b1496fab5095a696_zxe2w_307.jpg?rlvnet=1&rvtype=content'),
          fadeInDuration: Duration(milliseconds: 220),
        ),
      ),
    );
  }
}
